#import wtt
from class_wtt import TransformWTT
import scoring

import os
import numpy as np

import utils

trun = True
acc = 1e-3

qpar = [2, 2, 2, 3] # quantification parameter
nind = 1 # number of not used indeces counted from right
vedn = 16 # virtual effective dimensions
#None = no transposition, 'z' - z-order, and you can specify it as custom array
trsp = None #'z'

nn = [4**3, 4**3, 4**3, 3] # physical shape of modeled tensor
rk = 3 # rank
ee = np.arange(int(round(np.prod(nn)))) # our tensor in vector form
ee = ee.reshape(nn, order='F') # tensor with physical dimensions
qq = ee

w = TransformWTT(nn)
w.fit(
    qq,
    rank=rk,
    eps=0.,
    quant_par=qpar,
    transp=trsp,
    nuind=nind,
    virt_ef_dimnum=vedn
)
wqq = w.transform(qq)
if trun:
    wqq = list(map(lambda x: utils.hardthreshold(x, acc), wqq))
iwqq = np.squeeze(w.recover(wqq))
y = iwqq.flatten(order='F')
print(np.linalg.norm(qq - iwqq))

#qpar = 2
#qpar = [2,4,2,3]
qpar = [[2,4,2,4], [4,4,4], [2,2,2,2,2,2], [3]]
vedn = 13 #or None if qpar case 1 or 2
# note! joint axis is actually the last one and must be spicified with nuind value.
w = TransformWTT(nn[:-1])
w.fit(
    qq[:, :, :, 0],
    rank=rk,
    eps=0.,
    quant_par=qpar[:-1],
    transp=None,
    nuind=0,
    virt_ef_dimnum=vedn
)

# pull the (n-1) subtensor
nidx = [qq.ndim-1] + list(range(qq.ndim-1))

qqp = qq.transpose(nidx)[0]
wqq = w.transform(qq)
wqqp = w.transform(qqp)
if trun:
    wqqp = list(map(lambda x: utils.hardthreshold(x, acc), wqqp))
    wqq = list(map(lambda x: utils.hardthreshold(x, acc), wqq))
iwqq = w.recover(wqq)
iwqqp = w.recover(wqqp)
print(np.linalg.norm(iwqqp - qqp), np.linalg.norm(iwqq - qq))


w.fit(
    qq,
    rank=rk,
    eps=0.,
    quant_par=qpar[:-1],
    transp=None,
    nuind=0,
    virt_ef_dimnum=vedn
)

# pull the (n-1) subtensor
nidx = [qq.ndim-1] + list(range(qq.ndim-1))

qqp = qq.transpose(nidx)[0]
wqq = w.transform(qq)
wqqp = w.transform(qqp)
if trun:
    wqqp = list(map(lambda x: utils.hardthreshold(x, acc), wqqp))
    wqq = list(map(lambda x: utils.hardthreshold(x, acc), wqq))
iwqq = w.recover(wqq)
iwqqp = w.recover(wqqp)
print(np.linalg.norm(iwqqp - qqp), np.linalg.norm(iwqq - qq))
