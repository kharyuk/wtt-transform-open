import wtt
import wtt_utils
import numpy as np

class TransformWTT:
    def __init__(
        self,
        tensor_shape
    ):
        self.source_shape = tensor_shape
        self.source_dimnum = len(tensor_shape)
        
    def _fit_tensorization(self, tensor, quant_par, nuind, virt_ef_dimnum):
        '''
            tensorisation (quantization) settings block
         1) quant_par is integer - automatically compute virtual shape and
            number of effective dimensions
         2) quant_par is list of integers - automatically compute the sme
            parmeters for each axis
         3) quant_par is list of integer's lists - program would check
            given parameters (NOTE: virt_ef_num is required to be setted
            manually in this case) and if parameters are good it sets class
            class variables with them
        '''
        assert nuind < self.source_dimnum, "Incorrent _nuind_ value: nuind >= number of tensor dimensions"
        # if user specified a quant_par as integer:
        if isinstance(quant_par, int):
            assert quant_par > 1, "Bad quantization parameters: meaningless quantization value"
            tenlen = tensor.size
            assert tenlen % quant_par == 0, "Bad quantization parameter: blocks not conformed to its size"
            
            # let us compute virtual shape and number of efffective dimensions.
            self.virtual_shape, self.virtual_effective_dimnum = wtt_utils.quantize_all(
                self.source_shape, quant_par, nuind
            )
            
        # if user specified a quant_par as a set of integers
        # notice if a set is not aligned to source tensor's dimensions:
        else:
            assert len(quant_par) == self.source_dimnum, (
                "Bad quantization parameters: must have the same length"
                "as number of source tensor's dimensions"
            )
            # if all is good, then try to compute virtual shape for each dimension:
            # ATTENTION: if several dimensions need not quantization, place it in the end and set _nuind_!
            if isinstance(quant_par[0], int):
                self.virtual_shape = []
                self.virtual_effective_dimnum = 0
                # nuind = Not Used INDexes (of real tensor's shape)
                for i in range(len(quant_par)-nuind):
                    x = quant_par[i]
                    assert isinstance(x, int) and x > 1, (
                        "Bad quantization parameters: meaningless quantization value"
                    )
                    tmp1, tmp2 = wtt_utils.quantize_axis(self.source_shape[i], x)
                    self.virtual_shape.append(tmp1)
                    self.virtual_effective_dimnum += tmp2
                for i in range(len(quant_par)-nuind, len(quant_par)):
                    self.virtual_shape.append([quant_par[i]])
            else:
                # all other cases will be handled as case 3: all virtual dimensions are specified manually
                # user must specify _virt_ef_dimn_ variable:
                assert isinstance(virt_ef_dimnum, int), (
                    "Bad quantization parameters: for given set of quantization you"
                    " must define number of effective dimensions of virtualised tensor"
                )
                s = 0
                for i, x in enumerate(quant_par):
                    s += len(x)
                    assert np.prod(x) == self.source_shape[i], (
                        f"Bad quantization parameters: check subvector for axis number {i+1}"
                    )
                # check if there is a mistake with number of effective dimensions specified by user:
                assert virt_ef_dimnum <= s, (
                    "Bad quantization parameters: number of effective dimensions "
                    "can't be larger than number of all dimensions"
                )
                self.virtual_shape = quant_par
                self.virtual_effective_dimnum = virt_ef_dimnum
            
    def fit(
        self,
        tensor,
        rank=1,
        eps=0.,
        quant_par=2,
        transp=None,
        nuind=0,
        virt_ef_dimnum=None,
    ):
        self.eps = eps
        #nuind += max(0, tensor.ndim-self.source_dimnum)
        self._fit_tensorization(tensor, quant_par, nuind, virt_ef_dimnum)
        # transposition section
        virtmas = wtt_utils.list_d2s(self.virtual_shape)
        if transp is None:
            self.transp = np.arange(len(virtmas))
        elif transp == 'z':
            self.transp = wtt_utils.qtransposition(self.source_shape, self.virtual_shape, nuind)
        else:
            assert len(transp) == len(virtmas), (
                "Bad transposition parameter: not fit to virtual tensor shape"
            )
            self.transp = transp
        
        virtmas = wtt_utils.list_d2s(self.virtual_shape)
        sigma = self.transp
        if self.source_dimnum < tensor.ndim:
            virtmas += [-1]
            sigma = np.append(self.transp, len(self.transp))
        tmp = tensor.reshape(virtmas, order='F')
        tmp = tmp.transpose(sigma)
        self.ulist, self.rank, self.slist = wtt.computeSVDFilters(
            tmp, rank=rank, eps=self.eps, maxLevel=self.virtual_effective_dimnum,
            docopy=False, use_sv_truncation=True, return_sv=True, return_transformation=False
        )
        del tmp

    def transform(self, tensor):
        tens_ndim = tensor.ndim
        virtmas = wtt_utils.list_d2s(self.virtual_shape)
        sigma = self.transp
        if tens_ndim > self.source_dimnum:
            virtmas += [-1]
            sigma = np.append(self.transp, len(self.transp))
        T = np.reshape(tensor, virtmas, order='F')
        T = T.transpose(sigma)
        #trshp = wtt_utils.transp_shape(virtmas, sigma)
        T = wtt.WTT(T, self.ulist, self.rank, docopy=False)
        return T
        
    def recover(self, tensor):
        tens_size = sum(map(lambda x: x.size, tensor))
        expected_size = int(round(np.prod(self.source_shape)))
        virtmas = wtt_utils.list_d2s(self.virtual_shape)
        sigma = self.transp
        source_shape = self.source_shape
        if tens_size > expected_size:
            virtmas += [-1]
            sigma = np.append(self.transp, len(self.transp))
            source_shape = np.append(self.source_shape, -1)
        virtmas = wtt_utils.transp_shape(virtmas, sigma)
        iw = wtt.iWTT(
            tensor, [x.T for x in self.ulist[::-1]], self.rank, n=virtmas,
            docopy=True, result_tens=True
        )
        iw = iw.reshape(virtmas, order = 'F')
        sigma = wtt_utils.itransp(sigma)
        iw = iw.transpose(sigma)
        iw = iw.reshape(source_shape, order='F')
        return iw