###############################################################################
#                    Module for wtt decomposition for images.                 #
###############################################################################

from wtt3 import *
from pylab import *
import os
import Image


class WttIm(wtt): #image specialized wtt-class
    rk      = 1   #rank for wtt
    fnames  = []  #image file names
    dirname = []
    
    def __init__(self, dirname, rank=1, eps=0.00, nuind=0):  #initialization
        self.dirname = dirname
        self.fnames = immas(dirname)
        self.eps = eps
        self.rk = rank
        self.nuind = nuind
        return

    def openim(self,num):
        return imtoar(self.dirname + self.fnames[num])

    def dzembufilt(self, rank = 1, filtnum = 0, nf = None):  #count filters using all images
        imlist = imarlist([self.dirname + x for x in self.fnames]) 
        if (nf is not None):
            lh = len(nf)
            nf.append(lh)
        mas = array(imlist)
        frt = arange(1,mas.ndim+1)
        frt[mas.ndim-1] = 0
        mas = mas.transpose(frt)
        fnum = filtnum
        self.nuind = self.nuind + 1
        if (fnum != 0): fnum = fnum + 1
        wtt.__init__(self, mas, rank, self.eps, fnum, nf, self.nuind)
        self.truncate()
        self.nuind = self.nuind - 1
        del nf[-1]
#        print len(self.nmas)
        return

    #count filters usong one image
    def hitorifilt(self, imnum=0, rank=1, eps=0.00,filtnum=0,nf=None,nuind=0):
        A = imtoar(self.dirname + self.fnames[imnum])
        wtt.__init__(self,A,rank,self.eps,filtnum,nf,self.nuind)
        return

    #make wtt to all images
    def wttimf(self,acc=0):
        nms = []
        for x in self.fnames:
            nms.append(self.dirname + x)
        imlist = imarlist(nms)
        for i in range(0,len(imlist)):
            imlist[i] = self.wtt(imlist[i])
            imlist[i][abs(imlist[i])<acc]=0
        return imlist

    #vosstanovit all images
    def iwttimf(self, imlist, fnlist = []):
        if (fnlist == []):
            fnlist = self.fnames
        n = len(fnlist)
        for i in range(0,n):
#            print fnlist[i]
            fnlist[i] = fnlist[i].replace(".tif", "_wtt.tif")
#            print fnlist[i]
#            print
        i = 0
        for x in imlist:
            x = self.iwtt(x)
            artoim(x, self.dirname + fnlist[i])
            i = i+1
        return fnlist

    def wttfrm(self):
        os.remove(self.dirname + "*wtt*")
        return

    def psnrim(self,acc,num=0):
        A = self.openim(num)
        return self.psnr(A,acc)

    def crim(self, acc, num = 0):
        A = self.openim(num)
        return self.cr(A,acc)

    def rrim(self, acc1, acc2 = -1, num = 0):
        A = self.openim(num)
        return self.rr(A,acc1,acc2)

#all image data to np.array list
def imarlist(names):
    imlist = []
    for x in names:
        A = imtoar(x)
        imlist.append(A)
    return imlist

#local image to np.array
def imtoar(fn):
    Aim = Image.open(fn)
    A = array(Aim)
    A = A.reshape(A.shape,order='F')
    del Aim
    return A

#local np.array saving to fn-file as image
def artoim(A,fn):
    rescaled = (255.0 / A.max() * (A - A.min())).astype(np.uint8)
    Aim = Image.fromarray(rescaled)
    Aim.save(fn)
    del Aim
    return

#make list of all image file names in giving directory
def immas(dirname):
    fns = []
    l = os.listdir(dirname)
    for x in l:
        if ((x.endswith('.tif' ))):
            fns.append(x)
    fns.sort()
    fns = list(fns) 
#    i = 0
    #for x in fns:
     #   print str(i) + ")  ", x
      #  i = i+1
    #print
    return fns

#save all images in fs (names) as grayscaled
def rgbtogray(fs):#drnm,fs):
    for x in fs:    
#        x = drnm + x
        im=Image.open(x).convert("L")
#        x = x.replace("png","tif")
        artoim(array(im),x)
    return


###############################################################################
# puroguramu wa nagaku narita. dakara kommento o kakanakerebanaranai.         #
###############################################################################
#   Class wttim is used to do wtt-work with images.                           #
###############################################################################
# ======== Self-Variables:
#   > rk                  [int]  - rank for wtt
#   > nuind               [int]  - not used indexes in array decomposition
#   > fnames [list of char-str]  - list of images' filenames
#   > dirname        [char-str]  - name of directory with images
#==============================================================================
# ======== Methods:
#
#   > [np.array] openim(int num)
#           - open image as np.array
#           - returns image as np.array
#               * num = number of array in fnames-list
#
#   > [] dzembufilt(int filtnum = 0, list of int nf = [])
#           - count filters using all images.
#               * filtnum = how many filters to count
#               * nf = list for exchange to transpose
#
#   > [] hitorifilt (int imnum = 0,   int rank = 1, float eps = 0.01,
#                    int filtnum = 0, list of int nf = [], int nuind = 0 )
#           - count filters using one image
#               * imnum = number of image using to count filters
#               * rank = rank for wtt
#               * eps = accuracy for singular values (for rank correction)
#               * filtnum = how many filters to count
#               * nf = list for exchange to transpose
#               * nuind = not used indexes in array decomposition
#   
#   > [list of np.array] wttimf (float acc = 0)
#           - make wtt decomposition for all images in collection
#           - returns list of wtt-decomposed images
#               * acc = noise value for array's elements
#
#   > [list of str] iwttimf (list of np.array imlist,
#                                 list of str fnlist = [])
#           - recover images from imlist and saves them
#           - returns list of recovered file names
#             as "fnlist[i]" \ ".tif" + "_wtt.tif"
#               * imlist = list of wtt-decomposed images
#               * fnlist = list of filenames for imlist
#
#   > [] wttfrm ()
#           - remove all wtt-decomposed-recovered files (saved as "*.wtt*")
#
#   > [float] psnrim (float acc, int num = 0)
#           - count psnr value of image from fnames[i] file
#           - returns psnr value
#               * acc = noise value for array's elements
#               * num = image's number from fnames list
#
#   > [float] crim(float acc, int num = 0)
#           - count compress ratio for image
#           - returns compress ratio
#               * acc = noise value for array's elements
#               * num = image's number from fnames list
#
#   > [float] rrim(float acc1, float acc2 = -1, int num = 0)
#           - count recovery ratio for image
#           - returns recovery ratio
#               * acc1 = noise value for array's elements
#               * acc2 = recovering accuracy (-1 means that acc2 = acc1)
#               * num = image's number from fnames list
################################################################################
#
################################################################################
#  Subroutines for wtt-image work                                              #
################################################################################
#
#   > [list of np.array] imarlist (list of str names)
#           - make list of images from collection (as np.array)
#           - returns list of these images
#               * names = list of filenames
#
#   > [np.array ] imtoar (str fn)
#           - transport local image to np.array
#           - returns np.array representation of image
#               * fn = name of file with local image
#
#   > [] artoim (np.array A, str fn)
#           - transport local np.array to fn-file as image
#               * A = image as np.array
#               * fn = filename to save image
#
#   > [list of str] immas (str dirname)
#           - make list of all image file names in giving directory
#           - returns list of filenames in this directory
#               * dirname = directory name
#
#   > [] rgbtogray (list of str fs)
#           - save all images in fs (names) as grayscaled
#               * fs = list of images' filenames
################################################################################




