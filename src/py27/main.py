from wtt3 import *
import wtt3
from res import *
from vis import *
#from wavelets import *
import numpy as np

rang = (1,3,5,21,30,50)
eps1 = 0.00001 # for singular numbers in filter counting inside WTT
dirnm = "./Imgs1/"#coil-20-proc/"#random/"
filtimn = (0,1,2,3,4,5)
testimn = (4,5,6,7,8,9)
pltshp = (2,3)

imas = immas(dirnm)
l = len(imas)


rank = 12

filtnum = 7
testnum = 5

fnums = (0, 1, 2, 3, 4, 5)
tnums = (4, 5, 6, 7, 8, 9)

im = WttIm(dirnm,12)
im.hitorifilt(filtnum, rank)
nf = [0, 9, 1, 10, 2, 11, 3, 12, 4, 13, 5, 14, 6, 15, 7, 16, 8, 17]
inf = invert_nf(array(nf))
ranf = arange(len(im.nmas))

amin = 0.0
amax = 100.0
dacc = 5e-2
acclst = arange(amin, amax, dacc)

def ofmt():
    im.dzembufilt(nf = range(len(im.nmas)))
    all_ulist = im.ulist
    im.hitorifilt(filtnum, rank)
    hit_ulist = im.ulist
    im.dzembufilt(nf = nf)
    all_ulist_nf = im.ulist

    im.hitorifilt(filtnum, rank, nf=nf)
    hit_ulist_nf = im.ulist

    for x in tnums:
        A = im.openim(x)
        im.ulist = hit_ulist
        im.nf = ranf    

        xt = im.wtt(A)
        crlst = []
        psnrlst = []
        
        for acc in acclst:
            xt1 = xt.copy()
            xt1[abs(xt1) < acc] = 0
            ix = im.iwtt(xt1)
            crlst.append(im.cr(xt,acc))
            B = ix.reshape(A.shape)
            val = psnrcnt(A, B)
            psnrlst.append(val)
            #artoim(ix.reshape(A.shape), "qqq.tif")
        np.savetxt('filt='+str(filtnum)+'_test='+str(x)+'.txt', np.array((acclst, crlst, psnrlst)) )


        crlst = []
        psnrlst = []

        im.ulist = all_ulist
        im.nf = ranf    
        xt = im.wtt(A)
        for acc in acclst:
            xt1 = xt.copy()
            xt1[abs(xt1) < acc] = 0
            ix = im.iwtt(xt1)
            crlst.append(im.cr(xt,acc))
            B = ix.reshape(A.shape)
            val = psnrcnt(A, B)
            psnrlst.append(val)
            #artoim(ix.reshape(A.shape), "qqq.tif")
        np.savetxt('filt=all_test='+str(x)+'.txt', np.array((acclst, crlst, psnrlst)) )

        crlst = []
        psnrlst = []

        im.ulist = hit_ulist_nf
        im.nf = array(nf)
        xt = im.wtt(A)    
        for acc in acclst:
            xt1 = xt.copy()
            xt1[abs(xt1) < acc] = 0
            ix = im.iwtt(xt1)
            crlst.append(im.cr(xt,acc))
            B = ix.reshape(A.shape)
            val = psnrcnt(A, B)
            psnrlst.append(val)
            #artoim(ix.reshape(A.shape), "qqq.tif")
        np.savetxt('nf_filt='+str(filtnum)+'_test='+str(x)+'.txt', np.array((acclst, crlst, psnrlst)) )

        crlst = []
        psnrlst = []

        im.ulist = all_ulist_nf
        im.nf = array(nf)
        xt = im.wtt(A)    
        for acc in acclst:
            xt1 = xt.copy()
            xt1[abs(xt1) < acc] = 0
            ix = im.iwtt(xt1)
            crlst.append(im.cr(xt,acc))
            B = ix.reshape(A.shape)
            val = psnrcnt(A, B)
            psnrlst.append(val)
            #artoim(ix.reshape(A.shape), "qqq.tif")
        np.savetxt('nf_filt=all_test='+str(x)+'.txt', np.array((acclst, crlst, psnrlst)) )
    return

def mfot():
    A = im.openim(testnum)
    for x in fnums:
        im.dzembufilt(nf = range(len(im.nmas)))
        all_ulist = im.ulist
        im.hitorifilt(x, rank)
        hit_ulist = im.ulist
        im.dzembufilt(nf = nf)
        all_ulist_nf = im.ulist
        im.hitorifilt(x, rank, nf=nf)
        hit_ulist_nf = im.ulist
        
        im.ulist = hit_ulist
        im.nf = ranf    

        xt = im.wtt(A)
        crlst = []
        psnrlst = []
        for acc in acclst:
            xt1 = xt.copy()
            xt1[abs(xt1) < acc] = 0
            ix = im.iwtt(xt1)
            crlst.append(im.cr(xt,acc))
            B = ix.reshape(A.shape)
            val = psnrcnt(A, B)
            psnrlst.append(val)
            #artoim(ix.reshape(A.shape), "qqq.tif")
        np.savetxt('filt='+str(x)+'_test='+str(testnum)+'.txt', np.array((acclst, crlst, psnrlst)) )


        crlst = []
        psnrlst = []

        im.ulist = all_ulist
        im.nf = ranf    
        xt = im.wtt(A)
        for acc in acclst:
            xt1 = xt.copy()
            xt1[abs(xt1) < acc] = 0
            ix = im.iwtt(xt1)
            crlst.append(im.cr(xt,acc))
            B = ix.reshape(A.shape)
            val = psnrcnt(A, B)
            psnrlst.append(val)
            #artoim(ix.reshape(A.shape), "qqq.tif")
        np.savetxt('filt=all_test='+str(testnum)+'.txt', np.array((acclst, crlst, psnrlst)) )

        crlst = []
        psnrlst = []

        im.ulist = hit_ulist_nf
        im.nf = array(nf)
        xt = im.wtt(A)    
        for acc in acclst:
            xt1 = xt.copy()
            xt1[abs(xt1) < acc] = 0
            ix = im.iwtt(xt1)
            crlst.append(im.cr(xt,acc))
            B = ix.reshape(A.shape)
            val = psnrcnt(A, B)
            psnrlst.append(val)
            #artoim(ix.reshape(A.shape), "qqq.tif")
        np.savetxt('nf_filt='+str(x)+'_test='+str(testnum)+'.txt', np.array((acclst, crlst, psnrlst)) )

        crlst = []
        psnrlst = []

        im.ulist = all_ulist_nf
        im.nf = array(nf)
        xt = im.wtt(A)    
        for acc in acclst:
            xt1 = xt.copy()
            xt1[abs(xt1) < acc] = 0
            ix = im.iwtt(xt1)
            crlst.append(im.cr(xt,acc))
            B = ix.reshape(A.shape)
            val = psnrcnt(A, B)
            psnrlst.append(val)
            #artoim(ix.reshape(A.shape), "qqq.tif")
        np.savetxt('nf_filt=all_test='+str(testnum)+'.txt', np.array((acclst, crlst, psnrlst)) )
    return

def rank_kenkyuu():

    #nums = [(x,y),...], x - filtnum, y - testnum
    nums = [ [5, 0], [1, 1], [7, 1] ]
    def fpsnr(x,A,B):
        desired = 30.0
        regpar = 1e-14
        maxi = 255.0
        B1 = B.copy()
        B1[abs(B1) < x] = 0
        C = (im.iwtt(B1))
        D = C.reshape(A.shape)
        #artoim(B, 'q.png')
        B1 = A - D
        mse = ((B1**2).sum() + 0e0)/(B1.size + 0e0) + regpar
        psnrc = 20 * log10 (maxi / sqrt (mse) + regpar)
        return desired - psnrc

    ranks = range(1,31)
    a = 10.0
    b = 200.0
    for nm in nums:


        crlst_a = []
        crlst_f = []
        acc_a_lst = []
        acc_f_lst = []

        for r in ranks:
            im.nf = array(nf)
            im.hitorifilt(nm[0], r, nf=nf)
            A = im.openim(nm[1])
            xt = im.wtt(A)
            xt1 = xt.copy()    
            fun_psnr = lambda x: fpsnr(x,A,xt)
            acc = brentq(fun_psnr,a,b)
            crlst_f.append(im.cr(xt1,acc))
            acc_f_lst.append(acc)

            im.nf = array(nf)
            im.dzembufilt(r, nf = nf)
            A = im.openim(nm[1])
            xt = im.wtt(A)    
            xt1 = xt.copy()
            fun_psnr = lambda x: fpsnr(x,A=A,B=xt)
            #print fun_psnr(a, 0.1), fun_psnr(b,0.1)
            acc = brentq(fun_psnr,a,b)
            crlst_a.append(im.cr(xt1,acc))
            acc_a_lst.append(acc)

        np.savetxt('ranks_nf_filt='+str(nm[0])+'_test='+str(nm[1])+'.txt', np.array((acc_f_lst, crlst_f, ranks)) )
        np.savetxt('ranks_nf_filt=all'+'_test='+str(nm[1])+'.txt', np.array((acc_a_lst, crlst_a, ranks)) )
        
    return


#ofmt()
#mfot()
rank_kenkyuu()
