###############################################################################
#                     Module for WTT decomposition                            #
###############################################################################
from numpy import *
from mpi4py import MPI

class wtt:
    rank    = 0       #ranks
    ulist   = 0       #filters
    slist   = 0       #singular massives
    nmas    = 0       #dimensional massive
    dimn    = 0       #number of 2-dimensions
    shape   = 0       #true form of massive
    eps     = 0       #accuracy
    nf      = 0       #massive for transposition in q-tion
    filtnum = 0       #number of filters
    infms   = None
    nuind   = 0   #not used indexes in massive

    def __init__(self, A, r=1, eps=0.00, fnum=0, nf=None, nui=0):
        self.shape = A.shape
        self.dimn, self.nmas = bin_dim(A,nui)
        d = len(self.nmas)
        self.filtnum = fnum_comp(self.dimn,fnum,d)
        self.nf = array(nf_comp(nf,d))
        self.rank = r * ones(self.dimn)
        self.rank[0] = 1
        self.eps = eps
        self.filtcnt(A)
#        print len(self.nmas)
        return

    def truncate(self, nfilt = None):
        if (nfilt is None):
            nfilt = self.filtnum
        self.shape = self.shape[ :len(self.shape)-1]
        self.nmas = self.nmas[ :nfilt]
        self.filtnum = nfilt
        self.nf = self.nf[ :nfilt]
        self.ulist = self.ulist[ :nfilt]
        self.slist = self.slist[ :nfilt]
        return

    def filtcnt(self, A):
        A = A.reshape(self.nmas,order='F')
        A = transpose(A,self.nf)# made transpose, tsugi
        self.ulist, self.slist = ucount(A,self.rank, self.eps, self.nuind)
        return

    def wtt(self,A):
        B = array(A).copy()
        B = B.reshape(self.nmas,order='F')
        B = transpose(B,self.nf)# futatsume. dakara init no naka ni Achange koto ga dekinai
        B = B.reshape(self.nmas,order='F')
        B, self.infms = WTT(B,self.ulist,self.rank,self.nmas,0,self.filtnum)#sonomaeni self.dimn ga aru
        B = B.reshape(1,B.size,order='F')
        # made wtt, ovari
        return B

    def iwtt(self,A):      # recover massive
        B=A.copy()
        B = dWTT(B,self.ulist,self.rank,0,self.filtnum,self.infms)#sonomaeni self.dimn ga aru
        B = B.reshape(self.nmas,order = 'F')
        self.nf = invert_nf(self.nf)
        B = transpose(B,self.nf)
        B = B.reshape(self.shape,order='F')
        self.nf = invert_nf(self.nf)
        return B

    def psnr(self,A,acc):
        B = self.wtt(A)
        B[abs(B) < acc]=0
        B = self.iwtt(B)
        return psnrcnt(A,B)

    def cr(self, A, acc):
        B = self.wtt(A)
        B[abs(B) < acc]=0
        nnzA = (A!=0).sum()
        nnzB = (B!=0).sum()
        k = ( nnzA + 0e0) / ( nnzB + 0e0)
        return k

    def rr(self, A, acc1, acc2 = -1):
        if ( acc2 < 0) :
            acc2 = acc1
        B = self.wtt(A)
        B[abs(B) < acc1]=0
        B = self.iwtt(B)
        C = (A-B)
        k = (abs(C) < acc2).sum()
        k = (k + 0e0) / (A.size)
        return k
        

def countnf(dim):                       #compute form: han->/<-chyo
    nf = trunc(zeros(dim))
    j = 0
    for i in range(0,dim-1):
        if (not(i%2)):
            nf[i] = j
            j =j+1
    j = dim-2
    for i in range(0,dim-1):
        if (i%2):
            nf[i] = j
            j = j-1
    nf[dim-1] = dim-1
    return nf

def invert_nf(nf):                  #invert form
    inv = ones(nf.size)
    for i in range(nf.size):
        inv[nf[i]]=i
    return inv

def ucount(A,r,eps = 0, nui=0):                    #filters computation
    d = A.ndim
    N = A.size
    n = A.shape
    nid = 1
    if ((nui>0)&(nui<d)):
        d = d - nui
        nid = array(n)[-nui:].prod()
    ulist=[]
    slist=[]
    temp = A.reshape([n[0],N/n[0]],order='F')
    for k in range(0,d):
        if (temp.shape[0]>temp.shape[1]): #!!
            U,S,V=linalg.svd(temp,full_matrices=True)
        else:
            U,S,V=linalg.svd(temp,full_matrices=False)
        ulist.append(U)
        slist.append(S)
        S[abs(S)<eps * abs(S).max()] = 0 #new corrective
        f = (S!=0).sum()
        [m,p]=shape(temp)
        if (k!=d-1):
            R=min(m,p/nid,int(r[k+1]),f)#/new corrective
            temp=dot(transpose(U),temp)
            temp=temp[0:R,:]
            N = temp.size
            temp=temp.reshape([n[k+1]*R,N/(R*n[k+1])],order='F')
            r[k+1]=R##here was terrible error: not k, but k+1; thr main mistake of all work
    return ulist,slist


def WTT(A,ulist,r,n,k,d):               #WTT
    B = empty(A.size)
    uk = 0
    infms = []
    infms.append(uk)
    w = A
    for k in xrange(d):
        N = w.size
        w = w.reshape(ulist[k].shape[0],N/ulist[k].shape[0],order='F')
        w = dot(ulist[k].T,w)
        if (k < (d-1)):
            q = w[r[k+1] : , : ].size
            B[uk : uk + q] = w[r[k+1] : , :].flatten(order='F').copy()
            uk = uk + q
            infms.append(uk)
            w = w[0:r[k+1],:]
        else:
            B[uk : ] = w.flatten(order='F').copy()
    return B, infms

def dWTT(A,ulist,r,k,d, infms):                #dWTT
    w = A.flatten(order='F')
    idx = 1
    for k in xrange(d-1,-1,-1):
        if (k < d-1): 
            idx1 = w[ infms[k  ] : ].size
            idx2 = w[ infms[k+1] : ].size
            q = empty(idx1).reshape(ulist[k].shape[1], idx1/ulist[k].shape[1], order = 'F')
            shp1 = q[ : r[k+1], : ].shape
            shp2 = q[r[k+1] : , : ].shape
            q[ : r[k+1], : ] = w[-idx2 :].reshape(shp1, order='F')
            q[r[k+1] : , : ] = w[-idx1 : -idx2].reshape(shp2,order='F')
            q = dot(ulist[k],q)
            w[-idx1 : ] = q.flatten(order='F').copy()            
        else:
            idx = w[ infms[k] : ].size
            q = w[-idx : ].reshape(ulist[k].shape[1], idx/ulist[k].shape[1],order='F')
            q = dot(ulist[k],q)
            w[-idx : ] = q.flatten(order='F').copy()
    return w

def psnrcnt(ar1, ar2, maxi = 255.0):
    sh = ar1.shape
    if (ar2.shape!=sh):
        print "PSNR: shape mismatch"
    mn = 1
    for x in sh:
        mn = mn * x
    mse = ( ( (ar1-ar2)**2 ).sum() ) / mn
    psnr = 20 * log10 (maxi / sqrt (mse) )
    return psnr

def bin_dim(M,nuind=0):
    alln = M.size
    d = M.ndim
    for i in range(0,nuind):
        alln = alln/M.shape[d-i-1]
    n = alln
    k = 0
    while ((n!=0)&(n%2==0)):           #computing k from 2^k
        k = k+1
        n = n/2
    n = alln
    ost = n/(2**k)
    t = 0
    if (ost!=1):
        t = 1
    dim = [2]*(k+t+nuind)
    if (t!=0):
        dim[k] = ost
    q = len(dim)
    for i in range(0,nuind):
        dim[q-1-i] = M.shape[d-i-1]
    return k, dim

def nf_comp(nf, d):
    if (nf is None):
        nf = range(0,d)
    else:
        if (len(nf)!=d):
            nf = arange(0,d)
#            print "Bad dimension for exchange"
    return nf

def fnum_comp(dimn,fnum,d):
    a = dimn
    if ( (fnum > 0) & (fnum < d) ):
        a = fnum
#    print "Filters to count: ", a
    return a

def fflag(qq):
    return qq.flags.f_contiguous


###############################################################################
#      Class wtt                                                              #
###############################################################################
# ========  Self-variables:
#
#   > rank                [np.array]  - rank of filters
#   > ulist       [list of np.array]  - list of filters
#   > slist       [list of np.array]  - list of singular values
#   > nmas             [list of int]  - dimensional array
#   > dimn                     [int]  - number of 2-dimensions
#   > shape                  [tuple]  - true form of array
#   > eps                    [float]  - accuracy for singular values
#   > nf                  [np.array]  - array for transposition in exchange
#   > filtnum                  [int]  - number of filters
# =============================================================================
# ========  Methods :
#
#   > [] filtcnt (np.array A)
#           - count filters
#               * A = array for wtt-decomposition
#
#   > [np.array] wtt (np.array B)
#           - make WTT-decomposition for array
#           - returns pseudo-sparsed array
#               * B = array for wtt-decomposition
#
#   > [np.array] iwtt (np.array B)
#           - recover array
#           - returns recovered array
#               * B = array for recovering
#
#   > [float] psnr (np.array A, float acc)
#           - count PSNR-value for giving array and accuracy
#           - returns RSNR-value
#               * A = array for kenkyuu
#               * acc = accuracy for wtt-decomposition result
#
#   > [float] cr (np.array A, float acc)
#           - compute compress rate of WTT
#           - returns compress rate
#               * A = testing array
#               * acc = accuracy for WTT-decomposition
#
#   > [float] rr (np.array A, float acc1, float acc2 = -1)
#           - compute recovery rate of WTT
#           - returns recovery rate
#               * A = testing array
#               * acc1 = accuracy for WTT-decomposition
#               * acc2 = recovering accuracy (-1 means that acc2 = acc1)
###############################################################################
#
###############################################################################
#        Subroutines for WTT-work                                             #
###############################################################################
#
#   > [np.array] countnf (int dim)
#           - compute form han-choyo form ( like (0,1,2,3,4) -> (0,3,2,1,4) )
#           - returns exchange array
#               * dim = number of dimensions (eq. size of returning array)
#
#   > [np.array] invert_nf (np.array nf)
#           - invert nf array ( like (0,3,2,1,4) -> (0,3,2,4,1) )
#           - returns inverted array
#               * nf = array to invert
#
#   > [list of np.array,
#      list of np.array] ucount (np.array A, int r, int eps = 0,
#                                int filtnum = 0)
#           - count filters for wtt-decomposition
#           - returns list of filters and list of singular values
#             from SVD-decomposition
#               * A = array for decomposition
#               * r = rank for WTT
#               * eps = accuracy for singular values
#               * filtnum = number of filters to compute
#
#   > [np.array] WTT (np.array A, list of np.array ulist, np.array r,
#                     int n, int k, int d)
#           - compute WTT-decomposition
#           - returns pseudo-sparsed array
#               * A = array for decomposition
#               * ulist = list of filters
#               * r = array of ranks for filters
#               * n = list of dimensions for 2**d-form of array
#               * k = start filter number
#               * d = final filter number
#
#   > [np.array] dWTT (np.array A, list of np.array ulist, np.array r,
#                      int k, int d)
#           - recover compressed array
#           - returns recovered array
#               * A = array for recovery
#               * ulist = list of filters
#               * r = array of ranks for filters
#               * k = start filter number
#               * d = final filter number
#
#   > [double] psnrcnt (np.array ar1, np.array ar2, double maxi = 255.0):
#           - compute PSNR value
#           - returns PSNR value
#               * ar1 = first array
#               * ar2 = second array
#               * maxi = maximum pixel value that he can get
#
#   > [int, tuple] bin_dim (np.array M, int nuind = 0)
#           - compute d from 2**d-representation of array, make tuple of
#             giving array dimensions ( like (4,4,3) -> (2,2,2,2,3) )
#           - returns number d and tuple of dimensions
#               * M = array for dimensional work
#               * nuind = not used indexes in true array's shape
#
#   > [list of int] nf_comp (list of int nf, int d)
#           - check transposition form for exchange
#           - returns transposition form
#               * nf = form for checking
#               * d = maximal length of form
#
#   > [int] fnum_comp (int dimn, int fnum, int d):
#           - check number of filters to count
#           - returns number of filters to count
#               * dimn = number of binary dimensions
#               * fnum = desired number of filters to count
#               * d = maximal number of binary dimensions
#
#   > [bool] fflag (np.array qq)
#           - check if the array has an fortran order
#           - returns flag of fortran order
#               * qq = array for checking
###############################################################################

