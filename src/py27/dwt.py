import pywt
import numpy as np

nstep = lambda m: int(np.log2(m))

def kerdwt(A, wtype):
    assert A.ndim == 1
    N = A.size
    [cA, cD] = pywt.dwt(A, wtype, 'per')
    B = np.hstack( (cA, cD) )
    return B
    
def keridwt(A, wtype):
    assert A.ndim == 1
    [cA, cD] = np.hsplit(A, 2)
    B = pywt.idwt(cA, cD, wtype, 'per')
    return B

def kerdwt2(A, wtype):
    assert A.ndim == 2
    (ll, (lh, hl, hh)) = pywt.dwt2(A, wtype, 'per')
    tmp1 = np.hstack( (ll, lh) )
    tmp2 = np.hstack( (hl, hh) )
    B = np.vstack( (tmp1, tmp2) )
    return B

def keridwt2(A, wtype):
    assert A.ndim == 2
    [tmp1, tmp2] = np.vsplit(A, 2)
    [ll, lh] = np.hsplit(tmp1, 2)
    [hl, hh] = np.hsplit(tmp2, 2)
    B = pywt.idwt2((ll, (lh, hl, hh)), wtype, 'per')
    return B
    
def kerdwt3(A, wtype):
    assert A.ndim == 3
    B = A.copy()
    [m, n, l] = B.shape
    for j in range(n):
        for k in range(l):
            B[:, j, k] = kerdwt(B[:, j, k], wtype)
    for i in range(m):
        for k in range(l):
            B[i, :, k] = kerdwt(B[i, :, k], wtype)
    for i in range(m):
        for j in range(n):
            B[i, j, :] = kerdwt(B[i, j, :], wtype)
    return B

def keridwt3(A, wtype):
    assert A.ndim == 3
    B = A.copy()
    [m, n, l] = B.shape
    for i in range(m):
        for j in range(n):
            B[i, j, :] = keridwt(B[i, j, :], wtype)
    for i in range(m):
        for k in range(l):
            B[i, :, k] = keridwt(B[i, :, k], wtype)
    for j in range(n):
        for k in range(l):
            B[:, j, k] = keridwt(B[:, j, k], wtype)
    return B

def kerdwt4(A, wtype):
    assert A.ndim == 4
    B = A.copy()
    [m, n, l, p] = B.shape
    for j in range(n):
        for k in range(l):
            for q in range(p):
                B[:, j, k, q] = kerdwt(B[:, j, k, q], wtype)
    for i in range(m):
        for k in range(l):
            for q in range(p):
                B[i, :, k, q] = kerdwt(B[i, :, k, q], wtype)
    for i in range(m):
        for j in range(n):
            for q in range(p):
                B[i, j, :, q] = kerdwt(B[i, j, :, q], wtype)
    for i in range(m):
        for j in range(n):
            for k in range(l):
                B[i, j, k, :] = kerdwt(B[i, j, k, :], wtype)
    return B

def keridwt4(A, wtype):
    assert A.ndim == 4
    B = A.copy()
    [m, n, l, p] = B.shape
    for i in range(m):
        for j in range(n):
            for k in range(l):
                B[i, j, k, :] = keridwt(B[i, j, k, :], wtype)
    for i in range(m):
        for j in range(n):
            for q in range(p):
                B[i, j, :, q] = keridwt(B[i, j, :, q], wtype)
    for i in range(m):
        for k in range(l):
            for q in range(p):
                B[i, :, k, q] = keridwt(B[i, :, k, q], wtype)
    for j in range(n):
        for k in range(l):
            for q in range(p):
                B[:, j, k, q] = keridwt(B[:, j, k, q], wtype)
    return B

def dwt4(A, wtype):
    assert A.ndim == 4
    [m, n, l, p] = A.shape
    nst = nstep(m)
    B = A.copy()
    for i in range(nst):
        rind = m // (2**i)
        B[:rind, :rind, :rind, :rind] = kerdwt4(B[:rind, :rind, :rind, :rind], wtype)
    return B

def idwt4(A, wtype):
    assert A.ndim == 4
    m = A.shape[0]
    n = nstep(m)
    B = A.copy()
    for i in range(n):
        rind = m // (2**(n-i-1))
        B[:rind, :rind, :rind, :rind] = keridwt4(B[:rind, :rind, :rind, :rind], wtype)
    return B


def dwt3(A, wtype):
    assert A.ndim == 3
    [m, n, l] = A.shape
    nst = nstep(m)
    B = A.copy()
    for i in range(nst):
        rind = m // (2**i)
        B[:rind, :rind, :rind] = kerdwt3(B[:rind, :rind, :rind], wtype)
    return B

def idwt3(A, wtype):
    assert A.ndim == 4
    m = A.shape[0]
    n = nstep(m)
    B = A.copy()
    for i in range(n):
        rind = m // (2**(n-i-1))
        B[:rind, :rind, :rind] = keridwt3(B[:rind, :rind, :rind], wtype)
    return B

def dwt2(A, wtype):
    assert A.ndim == 2
    m = A.shape[0]
    n = nstep(m)
    B = A.copy()
    for i in range(n):
        rind = m // (2**i)
        B[:rind, :rind] = kerdwt3(B[:rind, :rind], wtype)
    return B

def idwt2(A, wtype):
    assert A.ndim == 2
    m = A.shape[0]
    n = nstep(m)
    B = A.copy()
    for i in range(n):
        rind = m // (2**(n-i-1))
        B[:rind, :rind] = keridwt3(B[:rind, :rind], wtype)
    return B
    
