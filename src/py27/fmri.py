import numpy as np
import pylab as pl
import nibabel as nib
import os

import dicom
from utils import block_view

def binarise(a, nix = None):
    ba = a.copy()
    shp = list(a.shape)
    for i in xrange(len(shp)):
        if nix is not None:
            if i in nix: continue
        l = shp[i]
        b = np.log2(l)
        n = int(2**np.ceil(b))
        if (n > l):
            nshp = shp
            nshp[i] = n - l
            zrs = np.zeros(nshp)
            ba = np.concatenate((ba, zrs), axis = i)
            shp = list(ba.shape)
    return ba

def get_fnms(dirnm, ext):
    allfiles = os.listdir(dirnm)
    fnms = filter(lambda x: x.endswith(ext), allfiles) 
    return fnms

def loadimg(fn):
    fnm = fn
    if not fnm.endswith(".img"):
        fnm = fn + ".img"
    fmrid = nib.load(fnm)
    data = fmrid.get_data()
    npdat = np.array(data)
    del data
    del fmrid
    return npdat

def loadima(fn, retcl = False):
    fnm = fn
    if not (fnm.endswith(".ima") or fnm.endswith(".IMA")):
        fnm = fn + ".ima"
    fmrid = dicom.read_file(fnm)
    data = fmrid.pixel_array
    if retcl:
        return data, fmrid
    del fmrid
    return data

def print_fmri(data, filename, mask = True):
    plt.clf()
    if len(data.shape) > 3: mean_img = np.mean(data, axis=3)
    else: mean_img = data
    n = mean_img.shape[2]
    if (mask):
        mask = compute_epi_mask(mean_img)
        mean_img *= mask
        filename = filename + "masked"
    for i in xrange(n):
        print i
        plt.subplot(10, 6, n-i)
        pl.axis('off')
        plt.imshow(np.rot90(mean_img[:, :, i]),#interpolation='nearest',
                  cmap=pl.cm.Spectral)

    plt.savefig("./"+filename+".pdf")

def quant_ima(a, struct = [1,1], sgnf = 1):
    xl = a.shape[0] / struct[0]
    yl = a.shape[1] / struct[1]
    lst = None
    i_last = sgnf / struct[1]
    j_last = sgnf % struct[1]
    for i in xrange(struct[0]):
        for j in xrange(struct[1]):
            if (i == i_last):
                if (j == j_last):
                    return lst
            c = block_view(a, (xl, yl))[i, j]
            c = c.reshape((xl, yl, 1))
            if lst is None:
                lst = c
            else:
                lst = np.concatenate( (lst, c), axis = -1 )
    return lst

def load_all_files(directory, fnms, struct = [1,1], sgnf = 1):
    a = None
    for i in xrange(len(fnms)):
        tmp = loadima(directory + fnms[i])
        tmp = quant_ima(tmp, struct, sgnf)
        tmp = tmp.reshape(tmp.shape + (1,))
        if a is None:
            a = tmp
        else:
            a = np.concatenate((a, tmp), -1)
    return a

def plot_2D(arr):
    plt.clf()
    im = plt.imshow(arr, vmin = arr.min(), vmax = arr.max(),\
                    extent=[0, 1, 0, 1], interpolation = 'bilinear')
    plt.colorbar(im)
    return im

def coll_ima(a, struct = [1,1], sgnf = 1):
    true_shape = [a.shape[0] * struct[0], a.shape[1] * struct[1]]
    i_last = sgnf / struct[1]
    j_last = sgnf % struct[1]
    rv = np.zeros(true_shape, dtype = a.dtype)
    for i in xrange(struct[0]):
        for j in xrange(struct[1]):
            if (i == i_last):
                if (j == j_last):
                    return rv
            block_view(rv, (a.shape[0], a.shape[1]))[i,j] = a[:,:,i*struct[0] + j]
    return rv




if False:
    fmri_extension = ".IMA"
    directory = "../DASHA_1/"

    fnms = get_fnms(directory, fmri_extension)
    a = loadima(directory+fnms[0])
    shp = list(a.shape) + [1]
    a = a.reshape(shp)
    for l in fnms[1:]:
        b = loadima(directory+l)
        b = b.reshape(shp)
        a = np.concatenate( (a, b), axis = len(shp)-1)
    b = binarise(a, [2])
