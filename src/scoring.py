import numpy as np

def psnr(ar1, ar2, maxi=255.0):
    assert ar1.shape == ar2.shape, "PSNR: shape mismatch"
    mn = np.prod(ar1.shape)
    mse = ( np.linalg.norm(ar1-ar2)**2. ) / mn
    psnr = 20*np.log10(maxi / np.sqrt(mse))
    return psnr

def cr(tens):
    if isinstance(tens, list):
        nz = sum(map(lambda x: np.isclose(x, 0).sum(), tens))
        size_tens = sum(map(lambda x: x.size, tens))
        return size_tens / (size_tens-nz)
    return tens.size / (tens.size-np.isclose(tens, 0).sum())
        
    
    