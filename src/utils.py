import numpy as np

def hardthreshold(x, a):
    y = x.copy()
    y[np.abs(y) < a] = 0.
    return y

def direct_permute_batch_mode(tensor, batch_axis):
    d = tensor.ndim
    assert 0 <= batch_axis < d
    sigma = list(range(batch_axis)) + list(range(batch_axis+1, d))
    sigma += [batch_axis]
    return tensor.transpose(sigma)

def reversed_permute_batch_mode(tensor, batch_axis):
    d = tensor.ndim
    assert 0 <= batch_axis < d
    sigma = list(range(1, batch_axis)) + [0] + list(range(batch_axis, d))
    return tensor.transpose(sigma)

def linear_scaling(tensor, A=0, B=1):
    y = tensor - tensor.min()
    y = y * (A+B) / y.max()
    y = y - A
    return y