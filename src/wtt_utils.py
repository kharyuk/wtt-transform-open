import numpy as np

def itransp(tvec):
    '''
    invert transposition form vector for inverse transform
    '''
    itvec = np.empty(len(tvec), dtype='i')
    for i in range(len(tvec)):
        itvec[tvec[i]] = i
    return itvec

def qtransposition(true_shape, quant_shapes, nuind = 0):
    '''
    Function for transposition vector construction.
    For given tensor A of shape [i_1, i_2, ..., i_n, j_1, j_2, ..., ]
    it looks like [i_1, j_1, k_1, ..., i_2, j_2, ..., ]

    true_shape   - list of integers          - true shape of given tensor, without
                                               quantization
    quant_shapes - list of lists of integers - shape of quantized tensor
    '''
    # true dimensionality
    tr_dim = len(true_shape)
    # maximum dimensionality for each true axis
    qmax_dim = max([len(x) for x in quant_shapes])
    # dimensionality matrix
    dim_matrix = -1 * np.ones((tr_dim, qmax_dim), dtype='i')
    # fill it by rows
    offset = 0
    i = 0
    for i, x in enumerate(quant_shapes[:len(quant_shapes)-nuind]):
        dim_matrix[i, :len(x)] = np.arange(offset, offset+len(x))
        offset += len(x)
    # reshape fin vector by columns
    qtr = dim_matrix.flatten(order='F')
    # remove dummy entries
    qtr = qtr[qtr != -1]
    if (nuind > 0):
        tmp = np.arange(offset, offset+nuind)
        qtr = np.hstack((qtr, tmp))
    return qtr

def quantize_axis(len_ax, g):
    '''
    Function for axis quantization.

    len_ax - integer - length of given axis
    g      - integer - base for virtulization

    example:
    > quantize_axis(128, 4)
    >>      ([4, 4, 4, 2], 3)
    '''
    n = len_ax
    k = 0
    while ((n!=0) and (n % g == 0)):
        k += 1
        n = n/g
    n = int(n)
    qres = [g]*k
    if (n!=1):
        qres += [n]
    return qres, k

def quantize_all(tshp, base, nuind=0, consistent=False):
    qshp = []
    bcounter = 0
    for x in tshp[:len(tshp)-nuind]:
        qres, k = quantize_axis(x, base)
        qshp.append(qres)
        if consistent:
            bcounter += k
        else:
            bcounter += len(qres)
    for x in tshp[len(tshp)-nuind:]:
        qshp.append([x])
    return qshp, bcounter

def list_d2s(lst):
    rv = []
    for x in lst:
        rv += x
    return rv

def transp_shape(a, t):
    n = len(a)
    b = []
    for i in range(n):
        b.append(a[int(t[i])])
    return b

def fflag(qq):
    return qq.flags.f_contiguous