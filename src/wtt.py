import numpy as np
#import scipy.linalg

def reshape(T, shape):
    return np.reshape(T, shape, order='F')

def computeSVDFilters(
    T, rank=1, eps=None, maxLevel=None, docopy=True, use_sv_truncation=False,
    return_sv=False, return_transformation=False
):
    '''
    Routine for computing WTT filters as left singular matrices
    
    T = np.ndarray
        Input tensor of shape n.
    rank = int / np.ndarray / list / tuple; default: 1.
        WTT rank(s); to be corrected while filters are computing
    eps = float; default: None.
        Truncation constant for singular values
    maxLevel = integer; default: None
        Maximum level of decomposition, all dimensions after this will
        be skipped.
    docopy = boolean; default: True
        Specifies whether make copy of input tensor or not.
    use_sv_truncation = boolean; default: False
        Either use truncation condition for singular values or not;
        If chosen, |s_i| < eps*|s_max| will be set to zero. It leads
        to lower rank estimation.
    return_sv = boolean; default: False
        Specifies whether return singular values associated with each filter or not.
    return_transformation = boolean; default: False
        Specifies whether return result of input transformation or not
    '''
    d = T.ndim
    n = T.shape

    if eps is not None:
        assert isinstance(eps, float), "Non-float eps parameter"
    else:
        eps = np.spacing(1.)
    if maxLevel is not None:
        assert isinstance(maxLevel, int), "Non-integer maxLevel parameter"
        assert 0 < maxLevel <= d, "Invalid maxLevel parameter"
        numberOfFilters = min(maxLevel, d)
    else:
        numberOfFilters = d
    if isinstance(rank, int):
        r = np.ones(numberOfFilters, dtype='i')
        r[1:] = rank
    elif isinstance(rank, (tuple, list)):
        r = np.array(rank)
    elif isinstance(rank, np.ndarray):
        r = rank.flatten()
        r = r.astype('i')
    assert len(r) == numberOfFilters, "There should be %d ranks, not %d" % (
        numberOfFilters, len(r)
    )
    assert r[0] == 1, "The 1st rank is always 1, not %d" % (r[0])
    
    filters = []
    if return_sv:
        singularValues = []
    
    if docopy:
        tmp = T.copy()
    else:
        tmp = T
        
    if return_transformation:
        Y = []

    
    #_relaxCTimeConst = 2.
    #_relaxCTimeConstSize = 2.
    
    for k in range(numberOfFilters):
        nRows = int(round(n[k]*r[k]))
        tmp = reshape(tmp, [nRows, -1])
        nCols = tmp.shape[1]
        fullMatricesFlag = nRows > nCols
        #if _relaxCTimeConst*nRows < nCols:
        
        u, s, vt = np.linalg.svd(tmp, full_matrices=fullMatricesFlag)
        # u, s, vt = scipy.linalg.svd(tmp, full_matrices=fullMatricesFlag)
        filters.append(u.T)
        tmp = np.dot(u.T, tmp)
        # truncate condition: |s| < eps*|s_max|
        nnzSV = np.sum(np.abs(s) >= (eps*np.abs(s).max()))
        if return_sv:
            singularValues.append(s[:nnzSV])
        if k < numberOfFilters-1:
            newRank = min(r[k+1], nRows, nCols)
            if use_sv_truncation:
                newRank = min(newRank, nnzSV)
            r[k+1] = int(newRank)
            if return_transformation:
                Y = [tmp[r[k+1]:, :]] + Y
            tmp = tmp[:r[k+1], :]
    return_values = [filters, r]
    if return_transformation:
        return_values.append(Y)
    if return_sv:
        return_values.append(singularValues)
    return tuple(return_values)
    
def randomizedNormalFilters(
    T, rank=1, mu=0., sigma=1., maxLevel=None, docopy=True, return_inversed=True,
    return_transformation=False
):
    '''
    Routine for sampling WTT filters from N(\mu, \sigma^2)
    
    T = np.ndarray
        Input tensor of shape n.
    rank = int / np.ndarray / list / tuple; default: 1.
        WTT rank(s); to be corrected while filters are computing
    mu = float; default: 0.
        Mean of Gaussian distribution
    sigma = float; default: 1.
        Standard deviation of Gaussian distribution
    maxLevel = integer; default: None
        Maximum level of decomposition, all dimensions after this will
        be skipped.
    docopy = boolean; default: True
        Specifies whether make copy of input tensor or not.
    return_inversed = boolean; default: True
        Either return inversed filters or not.
    return_transformation = boolean; default: False
        Specifies whether return result of input transformation or not
    '''
    d = T.ndim
    n = T.shape
    
    if maxLevel is not None:
        assert isinstance(maxLevel, int), "Non-integer maxLevel parameter"
        assert 0 < maxLevel <= d, "Invalid maxLevel parameter"
        numberOfFilters = min(maxLevel, d)
    else:
        numberOfFilters = d
    
    if isinstance(rank, int):
        r = np.ones(numberOfFilters, dtype='i')
        r[1:] = rank
    elif isinstance(rank, (tuple, list)):
        r = np.array(rank)
    elif isinstance(rank, np.ndarray):
        r = rank.flatten()
        r = r.astype('i')
    assert len(r) == numberOfFilters, "There should be %d ranks, not %d" % (
        numberOfFilters, len(r)
    )
    assert r[0] == 1, "The 1st rank is always 1, not %d" % (r[0])
    
    directFilters = []
    if return_inversed:
        inverseFilters = []
    
    if docopy:
        tmp = T.copy()
    else:
        tmp = T
        
    if return_transformation:
        Y = []
    
    N = int(round(np.prod(n)))
    for k in range(numberOfFilters):
        nRows = int(round(n[k]*r[k]))
        nCols = N / nRows
        
        randomFilter = np.random.normal(loc=mu, scale=sigma, size=[nRows, nRows])
        directFilters.append(randomFilter)
        if return_inversed:
            inverseFilters.append(np.linalg.pinv(randomFilter))
        tmp = np.dot(randomFilter, tmp)
        if k < numberOfFilters-1:
            newRank = min(r[k+1], nRows, nCols)
            r[k+1] = int(newRank)
            if return_transformation:
                Y = [tmp[r[k+1]:, :]] + Y
            tmp = tmp[:r[k+1], :]
    return_values = [directFilters]
    if return_inversed:
        return_values.append(inverseFilters)
    return_values.append(r)
    if return_transformation:
        return_values.append(Y)
    return tuple(return_values)
        

def WTT(T, directFilters, ranks, docopy=True):
    '''
    Wavelet Tensor Train transform.
    
    T = np.ndarray
        Tensorized signal to be transformed
    directFilters = list / tuple (of np.ndarrays)
        Filter bank for direct transform
    ranks = np.ndarray / list / tuple
        Ranks of WTT decomposition
    docopy = boolean; default: True
        Specifies whether make copy of input tensor or not.
    '''
    d = T.ndim
    n = T.shape
    
    numberOfFilters = len(directFilters)
    
    assert 0 < numberOfFilters <= d, "Invalid number of filters"
    
    r = np.array(ranks, dtype='i')
    assert len(r) == numberOfFilters, "There should be %d ranks, not %d" % (
        numberOfFilters, len(r)
    )
    assert r[0] == 1, "The 1st rank is always 1, not %d" % (r[0])
    
    if docopy:
        tmp = T.copy()
    else:
        tmp = T
    
    transformedT = []
    for k in range(numberOfFilters):
        nRows = int(round(n[k]*r[k]))
        tmp = reshape(tmp, [nRows, -1])
        nCols = tmp.shape[1]
        tmp = np.dot(directFilters[k], tmp)
        if k < numberOfFilters-1:
            transformedT = [tmp[r[k+1]:, :]] + transformedT
            tmp = tmp[:r[k+1], :]
        else:
            transformedT = [tmp] + transformedT
    return transformedT

def iWTT(transformedT, inverseFilters, ranks, n=None, docopy=True, result_tens=True):
    '''
    Inverse of Wavelet Tensor Train transform.
    
    transformedT = list / tuple (of np.ndarrays)
        Parts of decomposed signal
    inverseFilters = list / tuple (of np.ndarrays)
        Filter bank for inverse transform. Order of filters is direct
        (i.e., the last is related to the 1st mode).
    ranks = np.ndarray / list / tuple
        Ranks of WTT decomposition
    n = np.ndarray / list / tuple; default: None
        Mode sizes of original tensor. If it is not specified, it will be
        estimated from available filters.
    docopy = boolean; default: True
        Specifies whether make copy of input or not.
    result_tens = boolean; default: True
        Either tensorize output or not.
    '''
    numberOfFilters = len(inverseFilters)
    r = np.array(ranks, dtype='i')
    assert len(r) == numberOfFilters, "There should be %d ranks, not %d" % (
        numberOfFilters, len(r)
    )
    assert r[0] == 1, "The 1st rank is always 1, not %d" % (r[0])
    nNoneFlag = n is None
    if not nNoneFlag:
        d = len(n)
    else:
        d = numberOfFilters
        n = [int(round(inverseFilters[d-i-1].shape[0]/r[i])) for i in range(d)]
    assert 0 < numberOfFilters <= d, "Invalid number of filters"
    

    
    
    if docopy:
        T = transformedT[0].copy()
    else:
        T = transformedT[0]
    
    for k in range(numberOfFilters):
        k_reversed = numberOfFilters-k-1
        T = np.dot(inverseFilters[k], T)
        T = reshape(T, [r[k_reversed], -1])
        if k_reversed > 0:
            if r[k_reversed-1]*n[k_reversed-1] > r[k_reversed]:
                T = np.vstack([T, transformedT[k+1]])
    if result_tens:
        if nNoneFlag:
            additionalMode = int(round(T.size / np.prod(n)))
            n.append(additionalMode)
        T = reshape(T, n)
    return T



if __name__ == '__main__':
    d = 10
    n = [2]*d
    r = 20
    signal = np.sin(np.arange(2**d))
    #signal = np.ones(2**d)
    signal = np.reshape(signal, n, order='F')
    signal = np.random.uniform(size=(2, 2, 2, 2, 2, 2, 2, 2, 2, 5, 5, 5, 64))
    d = signal.ndim
    directFilters, ranks = computeSVDFilters(
        signal, rank=r, eps=None, maxLevel=d-1, docopy=True,
        use_sv_truncation=True, return_sv=False, return_transformation=False
    )
    inverseFilters = map(np.transpose, directFilters[::-1])
    
    decomposedSignal = WTT(signal, directFilters, ranks, docopy=True)
    recoveredSignal = iWTT(
        decomposedSignal, inverseFilters, ranks, n=None, docopy=True,
        result_tens=True
    )
    
    
    
    
    
    
    
    
    
    
    
    
    
