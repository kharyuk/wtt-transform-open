from class_wtt import TransformWTT
import scoring
import utils

import numpy as np

import matplotlib.pyplot as plt
import scipy.optimize


def cr_psnr_score_ranks(
    dataset,
    ranks,
    eps=0.,
    acc=None,
    test_num=None,
    train_num=None,
    transp='z',
    nuind=0,
    quant_par=2
):
    if acc is None:
        dacc = 5e-1
        acc = np.arange(0., 1e2+dacc, dacc)
    result_cr = np.empty([len(ranks), len(acc)])
    result_psnr = np.empty([len(ranks), len(acc)])
    n = dataset.shape[:-1]
    for i, rank in enumerate(ranks):
        w = TransformWTT(n)
        w.fit(
            dataset if train_num is None else dataset.T[train_num].T,
            rank=rank,
            eps=eps,
            quant_par=quant_par,
            transp=transp,
            nuind=nuind,
            virt_ef_dimnum=None
        )
        encoded = w.transform(
            dataset if test_num is None else dataset.T[test_num].T
        )
        for j, a in enumerate(acc):
            hat_enc = list(map(lambda x: utils.hardthreshold(x, a), encoded))
            result_cr[i, j] = scoring.cr(hat_enc)
            decoded = w.recover(hat_enc)
            result_psnr[i, j] = scoring.psnr(
                dataset if test_num is None else dataset.T[test_num].T,
                decoded
            )
    return result_cr, result_psnr, acc


def experiment1(
    dataset,
    objects,
    train_objects,
    test_objects,
    ranks,
    acc,
    eps=0.,
    quant_par=2,
    nuind=0
):
    train_nums = list(map(lambda x: objects.index(x), train_objects)) + [None]
    test_nums = list(map(lambda x: objects.index(x), test_objects))

    result_cr, result_psnr = [], []
    for i, train_num in enumerate(train_nums):
        result_cr_i, result_psnr_i = [], []
        for j, test_num in enumerate(test_nums):
            result_cr_ij, result_psnr_ij = [], []
            for k, transp in enumerate([None, 'z']):
                result_cr_ijk, result_psnr_ijk, _ = cr_psnr_score_ranks(
                    dataset=dataset,
                    ranks=ranks,
                    eps=eps,
                    acc=acc,
                    test_num=test_num,
                    train_num=train_num,
                    transp=transp,
                    nuind=nuind,
                    quant_par=quant_par
                )
                result_cr_ij.append(result_cr_ijk)
                result_psnr_ij.append(result_psnr_ijk)
            result_cr_i.append(result_cr_ij)
            result_psnr_i.append(result_psnr_ij)
        result_cr.append(result_cr_i)
        result_psnr.append(result_psnr_i)
    result_cr, result_psnr = np.array(result_cr), np.array(result_psnr)
    return result_cr, result_psnr

def plot_results_exp1(title_objects, result_cr, result_psnr, mode='a'):
    assert mode in ['a', 'b']
    nrows, ncols = 2, 3
    fig, ax = plt.subplots(nrows, ncols, figsize=(9, 6))
    for i in range(nrows):
        for j in range(ncols):
            k = j + i*ncols
            if mode == 'a':
                ax[i, j].plot(result_cr[0, k, 0], result_psnr[0, k, 0], 'b', label='Partial')
                ax[i, j].plot(result_cr[0, k, 1], result_psnr[0, k, 1], 'g', label='Partial (transp.)  ')
                ax[i, j].plot(result_cr[-1, k, 0], result_psnr[-1, k, 0], 'y', label='Joint')
                ax[i, j].plot(result_cr[-1, k, 1], result_psnr[-1, k, 1], 'r', label='Joint (transp.)  ')
            elif mode == 'b':
                ax[i, j].plot(result_cr[k, 0], result_psnr[k, 0], 'b', label='Partial')
                ax[i, j].plot(result_cr[k, 1], result_psnr[k, 1], 'g', label='Partial (transp.)  ')
                ax[i, j].plot(result_cr[-1, 0], result_psnr[-1, 0], 'y', label='Joint')
                ax[i, j].plot(result_cr[-1, 1], result_psnr[-1, 1], 'r', label='Joint (transp.)  ')
            else:
                raise ValueError
            ax[i, j].set_xlim([0., 30.])
            ax[i, j].set_ylim([20., 50.])
            ax[i, j].grid()
            ax[i, j].set_title(title_objects[k])
    fig.text(0.5, 0.04, "Compress Ratio", fontsize=14, color='black', ha='center')
    fig.text(0.04, 0.5, "PSNR", fontsize=14, color='black', va='center', rotation='vertical')
    handles, labels = ax[i, j].get_legend_handles_labels()
    fig.legend(handles, labels, bbox_to_anchor=(1.08, 0.5), loc='right')
    return fig, ax

def cr_rank_score_fixed_psnr(
    dataset,
    ranks,
    psnr_value,
    eps=0.,
    test_num=None,
    train_num=None,
    transp=None,
    nuind=0,
    quant_par=2,
    a=10,
    b=200
):
    result_acc = np.empty([len(ranks)])
    result_cr = np.empty([len(ranks)])
    n = dataset.shape[:-1]
    for i, rank in enumerate(ranks):
        w = TransformWTT(n)
        w.fit(
            dataset if train_num is None else dataset.T[train_num].T,
            rank=rank,
            eps=eps,
            quant_par=quant_par,
            transp=transp,
            nuind=nuind,
            virt_ef_dimnum=None
        )
        encoded = w.transform(
            dataset if test_num is None else dataset.T[test_num].T
        )
        def fun_psnr(acc, dummy):
            hat_enc = list(map(lambda x: utils.hardthreshold(x, acc), encoded))
            decoded = w.recover(hat_enc)
            current_psnr_value = scoring.psnr(
                dataset if test_num is None else dataset.T[test_num].T,
                decoded
            )
            return psnr_value - current_psnr_value
        acc = scipy.optimize.brentq(fun_psnr, a, b, 0.1)
        result_acc[i] = acc
        result_cr[i] = scoring.cr(list(map(lambda x: utils.hardthreshold(x, acc), encoded)))
    return result_acc, result_cr
    
def experiment2(
    dataset,
    objects,
    train_objects,
    test_objects,
    ranks,
    psnr_value,
    eps=0.,
    quant_par=2,
    nuind=0
):
    train_nums = list(map(lambda x: objects.index(x), train_objects)) + [None]
    test_nums = list(map(lambda x: objects.index(x), test_objects))

    result_acc, result_cr = [], []
    for i, train_num in enumerate(train_nums):
        result_acc_i, result_cr_i = [], []
        for j, test_num in enumerate(test_nums):
            result_acc_ij, result_cr_ij = [], []
            for k, transp in enumerate([None, 'z']):
                result_acc_ijk, result_cr_ijk = cr_rank_score_fixed_psnr(
                    dataset=dataset,
                    ranks=ranks,
                    psnr_value=psnr_value,
                    eps=eps,
                    test_num=test_num,
                    train_num=train_num,
                    transp=transp,
                    nuind=nuind,
                    quant_par=quant_par,
                    a=10,
                    b=200
                )
                result_acc_ij.append(result_acc_ijk)
                result_cr_ij.append(result_cr_ijk)
            result_acc_i.append(result_acc_ij)
            result_cr_i.append(result_cr_ij)
        result_acc.append(result_acc_i)
        result_cr.append(result_cr_i)
    result_acc, result_cr = np.array(result_acc), np.array(result_cr)
    return result_acc, result_cr

def plot_results_exp2(ranks, result_cr, train_objects, test_objects, z_order=True):
    plt.plot(ranks, result_cr[0, int(z_order)], 'g*-', label=train_objects[0])
    plt.plot(ranks, result_cr[-1, int(z_order)], 'r*-', label='Joint')
    plt.xlabel("Rank", fontsize=14, color='black')
    plt.ylabel("Compress Ratio", fontsize=14, color='black')
    plt.legend(title=f"Test image: {test_objects[0]}", loc="best")
    plt.grid()
    
def recover_compressed_dataset(
    dataset,
    rank,
    acc,
    train_num=None,
    test_num=None,
    quant_par=2,
    nuind=0,
    transp='z',
    eps=0.
):
    n = dataset.shape[:-1]
    w = TransformWTT(n)
    w.fit(
        dataset if train_num is None else dataset.T[train_num].T,
        rank=rank,
        eps=eps,
        quant_par=quant_par,
        transp=transp,
        nuind=nuind,
        virt_ef_dimnum=None
    )
    encoded = w.transform(
        dataset if test_num is None else dataset.T[test_num].T
    )
    encoded = list(map(lambda x: utils.hardthreshold(x, acc), encoded))
    decoded = w.recover(encoded)
    return decoded
